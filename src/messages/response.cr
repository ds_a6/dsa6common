require "./base_message"

module Common
  module Messages
    class Response < BaseMessage
      property success : Bool
      property message : String

      def initialize(success : Bool, message : String)
        @success = success
        @message = message
      end
    end
  end
end
