require "msgpack"

module Common
  module Messages
    abstract class BaseMessage
      include MessagePack::Serializable
    end
  end
end
