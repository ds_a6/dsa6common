require "./base_message"

module Common
  module Messages
    class FileRequest < BaseMessage
      property filename : String
      property contents : Bytes

      def initialize(filename : String, contents : Bytes)
        @filename = filename
        @contents = contents
      end
    end
  end
end
